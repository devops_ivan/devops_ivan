#!/bin/bash
#VARIABLES
 
current=$(date +"%Y")
re='[a-zA-Z]'
yy=$(date +"%Y")
mm=$(date "+%m")
dd=$(date "+%d")
 
#FONT COLOR
red=$'\e[1;31m'
NC="$(tput sgr0)"
 
#LOOP CONDITION
choice="Y"
while [[ $choice == "Y" || $choice == "y" ]]
do
  nameIsInvalid=0
  genderIsInvalid=0
  dayIsInvalid=0
  monthIsInvalid=0
  yearIsInvalid=0
while [[ "$nameIsInvalid" -eq 0 ]]
  do
   echo Enter your name:
    read -r name
     if ! [[ $name =~ ^[a-zA-Z]*$ ]]
       then
         echo  "${red}INVALID INPUT!! Please try again.${NC}"
       elif [[ -z $name ]]
       then
         echo  "${red}INVALID INPUT!! Please try again.${NC}"
       else
         nameIsInvalid=1
  fi
  echo ""
done
while [[ "$genderIsInvalid" -eq 0 ]]
 do
    echo "What is your gender?"
    echo "1.Male"
    echo "2.Female"
    echo "Enter here: "
     read -r gender
      if [[ $gender == 1 ]]
        then
            genderIsInvalid=1
            gender=Male
        elif [[ $gender == 2 ]]
          then
            genderIsInvalid=1
            gender=Female
          else
          echo  "${red}INVALID INPUT!! Please try again.${NC}"
 fi
 echo ""
done
while [[ "$yearIsInvalid" -eq 0 ]]
do
echo "${white}Enter Year:$NC"
read y
if [[ $y =~ ^[0-9]+$ ]]; then
  year=$y
  y=$(( $y % 4 ))
fi
if [[ $year -gt 2019  ]] || [[ $year -le 1819  ]]
  then
    echo "${red}INVALID INPUT!! Please try again.${NC}"
    echo ""
  elif [ $y -eq 0 ]
    then
         echo
       pyear=1
       yearIsInvalid=1
     else
          echo
        pyear=0
        yearIsInvalid=1
fi
done
echo "Choose Month: "
echo "[1]January [2]February [3]March [4]April [5]May [6]June"
echo "[7]July [8]August [9]September [10]October [11]November [12]December"
while [[ "$monthIsInvalid" -eq 0 ]]
do
echo "Enter Month: "
read month
if [[ $month == 2 ]]; then
  xmonth=February
  monthIsInvalid=1
  elif [[ $month == 1 ]]; then
    xmonth=January
    monthIsInvalid=1
  elif [[ $month == 3 ]]; then
    xmonth=March
    monthIsInvalid=1
  elif [[ $month == 4 ]]; then
    xmonth=April
    monthIsInvalid=1
  elif [[ $month == 5 ]]; then
    xmonth=May
    monthIsInvalid=1
  elif [[ $month == 6 ]]; then
    xmonth=June
    monthIsInvalid=1
  elif [[ $month == 7 ]]; then
    xmonth=July
    monthIsInvalid=1
  elif [[ $month == 8 ]]; then
    xmonth=August
    monthIsInvalid=1
  elif [[ $month == 9 ]]; then
    xmonth=September
    monthIsInvalid=1
  elif [[ $month == 10 ]]; then
    xmonth=October
    monthIsInvalid=1
  elif [[ $month == 11 ]]; then
    xmonth=November
    monthIsInvalid=1
  elif [[ $month == 12 ]]; then
    xmonth=December
    monthIsInvalid=1
      else
        echo "${red}INVALID INPUT!! Please try again.${NC}"
        echo ""
fi
echo ""
done
while [[ "$dayIsInvalid" -eq 0 ]]; do
  echo "Enter Day: "
  read day
if ! [[ $day =~ ^[0-9]+$ ]]; then
  echo "${red}INVALID INPUT!! Please try again.${NC}"
  echo ""
else
if [[ $xmonth == "February" ]] && [[ $pyear =~ 0 ]]
  then
    if [[ $day -gt 28 ]] || [[ $day -lt 1 ]]
      then
        echo "${red}INVALID INPUT!! IT'S NOT LEAP YEAR.${NC}"
        echo ""
      elif [[ $day =~ ^[0-9]+$ ]]; then
        dayIsInvalid=1
fi
fi
if [[ $xmonth == "February" ]] && [[ $pyear =~ 1 ]]
  then
if [[ $day -gt 29 ]] || [[ $day -lt 1 ]]
  then
    echo "${red}INVALID INPUT!! Please try again.${NC}"
    echo ""
elif [[ $day =~ ^[0-9]+$ ]]; then
    dayIsInvalid=1
fi
fi
if [[ $xmonth == "January" ]]
  then
if [[ $day -gt 31 ]] || [[ $day -lt 1 ]]
  then
    echo "${red}INVALID INPUT!! Please try again.${NC}"
    echo ""
elif [[ $day =~ ^[0-9]+$ ]]; then
    dayIsInvalid=1
fi
fi
if [[ $xmonth == "March" ]]
then
if [[ $day -gt 30 ]] || [[ $day -lt 1 ]]
  then
    echo "${red}INVALID INPUT!! Please try again.${NC}"
    echo ""
elif [[ $day =~ ^[0-9]+$ ]]; then
    dayIsInvalid=1
fi
fi
if [[ $xmonth == "April" ]]
  then
if [[ $day -gt 30 ]] || [[ $day -lt 1 ]]
  then
    echo "${red}INVALID INPUT!! Please try again.${NC}"
    echo ""
elif [[ $day =~ ^[0-9]+$ ]]; then
    dayIsInvalid=1
fi
fi
if [[ $xmonth == "May" ]]
  then
if [[ $day -gt 31 ]] || [[ $day -lt 1 ]]
  then
    echo "${red}INVALID INPUT!! Please try again.${NC}"
    echo ""
elif [[ $day =~ ^[0-9]+$ ]]; then
    dayIsInvalid=1
fi
fi
if [[ $xmonth == "June" ]]
  then
if [[ $day -gt 30 ]] || [[ $day -lt 1 ]]
  then
    echo "${red}INVALID INPUT!! Please try again.${NC}"
    echo ""
elif [[ $day =~ ^[0-9]+$ ]]; then
    dayIsInvalid=1
fi
fi
if [[ $xmonth == "July" ]]
  then
if [[ $day -gt 31 ]] || [[ $day -lt 1 ]]
  then
    echo "${red}INVALID INPUT!! Please try again.${NC}"
    echo ""
elif [[ $day =~ ^[0-9]+$ ]]; then
    dayIsInvalid=1
fi
fi
if [[ $xmonth == "August" ]]
  then
if [[ $day -gt 31 ]] || [[ $day -lt 1 ]]
  then
    echo "${red}INVALID INPUT!! Please try again.${NC}"
    echo ""
elif [[ $day =~ ^[0-9]+$ ]]; then
    dayIsInvalid=1
fi
fi
if [[ $xmonth == "September" ]]
  then
if [[ $day -gt 30 ]] || [[ $day -lt 1 ]]
  then
    echo "${red}INVALID INPUT!! Please try again.${NC}"
    echo ""
elif [[ $day =~ ^[0-9]+$ ]]; then
    dayIsInvalid=1
fi
fi
if [[ $xmonth == "October" ]]
  then
if [[ $day -gt 31 ]] || [[ $day -lt 1 ]]
  then
    echo "${red}INVALID INPUT!! Please try again.${NC}"
    echo ""
elif [[ $day =~ ^[0-9]+$ ]]; then
    dayIsInvalid=1
fi
fi
if [[ $xmonth == "November" ]]
  then
if [[ $day -gt 30 ]] || [[ $day -lt 1 ]]
  then
    echo "${red}INVALID INPUT!! Please try again.${NC}"
    echo ""
elif [[ $day =~ ^[0-9]+$ ]]; then
    dayIsInvalid=1
fi
fi
if [[ $xmonth == "December" ]]
  then
if [[ $day -gt 31 ]] || [[ $day -lt 1 ]]
  then
    echo "${red}INVALID INPUT!! Please try again.${NC}"
    echo ""
elif [[ $day =~ ^[0-9]+$ ]]; then
    dayIsInvalid=1
fi
fi
fi
done
if [ "$year" -le "$yy" ]
then
yyy=$(expr "$yy" - "$year")
mmm=$(expr "$mm" - "$month")
#ddd=`expr $dd - $d`
if [ "$month" -gt "$mm" ]
then
yyy=$(expr "$yyy" - 1)
mmm=$(expr "$mmm" + 12)
fi
if [ "$day" -gt "$dd" ]
then
ddd=$(expr "$day" - "$dd")
ddd=$(expr 31 - "$ddd")
else
ddd=$(expr "$dd" - "$day")
fi
fi
echo ""
echo "************************************************************************"
echo "Your name is $name, $yyy years old, $gender & Birthday is $xmonth $day $year"
echo "************************************************************************"
echo ""
read -p "Do you want to try it again? Y or N : " choice
done
