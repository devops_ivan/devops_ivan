#!/bin/bash
#START
Mdate="$(date +"%d-%m-%Y | %H-%M-%S")"
mysqldump -uroot -ppassword important_files > /opt/dump_backup/"$Mdate"important_files.sql
if [[ ! -f /opt/dump_backup.tar ]]; then
  cd /opt
  tar -cvf dump_backup.tar dump_backup
else
  cd /opt
  tar -uvf dump_backup.tar dump_backup
fi
 
#INCREMENTAL-BACKUP METHOD
TIME=`date +"%d-%m-%Y|%H-%M-%S"`
FILENAME=backup-Increment-$TIME.tar.gz
SRCDIR=dump_backup
DESDIR=incremental-backup
SNF=dump_backup/dump_backup.snar
cd /opt
tar -cvf $DESDIR/$FILENAME -g $SNF $SRCDIR
 
#FULL-BACKUP METHOD
if [[ ! -f /opt/full-backup/dump_backup-full.tar.gz ]]; then
  cd /opt
  tar -cvf dump_backup-full.tar.gz dump_backup && mv dump_backup-full.tar.gz /opt/full-backup/
else
  cd /opt
  tar -uvf /opt/full-backup/dump_backup-full.tar.gz dump_backup
fi
 
#EMAIL NOTIFICATION
echo "BACKUP SUCCESS DUMP_BACKUP USING INCREMENTAL METHOD AND FULL BACKUP METHOD" | mail -v -s "LEARNING MYSQL AND BACKUP SCRIPT" toro.ivan.manalo@gmail.com
