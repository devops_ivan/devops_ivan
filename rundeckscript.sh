#!/bin/bash

mkdir /opt/Installers
mkdir /opt/Apps
#Java
yum -y install wget
yum -y install epel-release
wget -c --header "Cookie: oraclelicense=accept-securebackup-cookie" http://download.oracle.com/otn-pub/java/jdk/8u131-b11/d54c1d3a095b4ff2b6607d096fa80163/jdk-8u131-linux-x64.tar.gz -P /opt/Installers &&
tar -xzf /opt/Installers/jdk-8u131-linux-x64.tar.gz -C /opt/Apps &&
/usr/sbin/alternatives --install /usr/bin/java java /opt/Apps/jdk1.8.0_131/bin/java 2
       alternatives --set java /opt/Apps/jdk1.8.0_131/bin/java 
       export JAVA_HOME=/opt/Apps/jdk1.8.0_131/bin/java

#NGINX

service httpd stop
systemctl disable httpd
yum -y install nginx &&
sudo systemctl start nginx
sudo firewall-cmd --permanent --zone=public --add-service=http 
sudo firewall-cmd --permanent --zone=public --add-service=https
sudo firewall-cmd --reload


#MySQL
wget http://repo.mysql.com/mysql-community-release-el7-5.noarch.rpm -P /opt/Installers &&
sudo rpm -ivh /opt/Installers/mysql-community-release-el7-5.noarch.rpm &&
sudo yum -y install mysql-server
sudo systemctl start mysqld

#Tomcat

wget http://mirror.rise.ph/apache/tomcat/tomcat-9/v9.0.19/bin/apache-tomcat-9.0.19.tar.gz -P /opt/Installers 
mkdir /opt/Apps/tomcat 
tar zxf /opt/Installers/apache-tomcat-9.0.19.tar.gz -C /opt/Apps/tomcat 
echo "export CATALINA_HOME=\"/opt/Apps/tomcat/apache-tomcat-9.0.19\"" >>~/.bashrc
source ~/.bashrc
chmod +x /opt/Apps/tomcat/apache-tomcat-9.0.19/bin/catalina.sh
chmod +x /opt/Apps/tomcat/apache-tomcat-9.0.19/bin/shutdown.sh
chmod +x /opt/Apps/tomcat/apache-tomcat-9.0.19/bin/startup.sh
firewall-cmd --zone=public --add-port=8080/tcp --permanent
sudo firewall-cmd --reload
cd /opt/Apps/tomcat/apache-tomcat-9.0.19/bin
sh startup.sh

