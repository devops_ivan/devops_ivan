#!/bin/bash
#VARIABLES
SSID=$(/System/Library/PrivateFrameworks/Apple80211.framework/Versions/Current/Resources/airport -I | awk -F': ' '/ SSID/ {print $2}')
#COLOR AND FONT
red=$'\e[1;31m'
blu=$'\e[1;34m'
NC="$(tput sgr0)"
bold=$'\x1b[1m'
function macinfo {
 
#DISPLAY OUTPUT
        echo $NC =========================================
        echo ${bold}$blu " MAC SYSTEM INFORMATIOM"
        echo $NC 1. Computer Name -$red $HOSTNAME
        echo $NC 2. User Name -$red `users`
        echo $NC 3." Network Name (SSID) -$red $SSID"
        echo $NC 4. RAM:${red} "`system_profiler SPHardwareDataType | grep " Memory:" | tail -c 5`${red}"
        echo $NC 5." Current Memory Usage - $red $(top -l 1 -s 0 | grep PhysMem | awk '{$2=$2/(1024^2); print $2,"GB";}')"
        echo $NC 6." Disk Storage -$red $(df -lH | grep /dev/disk0s2 | awk '{print $2,"G";}')"
        echo $NC 7." Current Disk Usage - $red $(df -lH | grep /dev/disk0s2 | awk '{print $5}') ($(df -lH | grep /dev/disk0s2 | awk '{print $2}')/$(df -lH | grep /dev/disk0s2 | awk '{print $3}'))"
        echo $NC 8. Total Number of CPU - ${red} `sysctl hw.ncpu`
        echo $NC 9." Current CPU Usage -$red $(ps -A -o %cpu | awk '{ cpu += $1 } END {print cpu}')%"
        echo $NC 10." Top processes -$red $(top -o cpu -l 2 -n 2 -stats pid,command,cpu | tail -n 6 | awk '{cpu = $2} END {print cpu}')"
        echo $NC -----------------------------------------
        echo ${bold}$blu " Scripted by: Ivan B. Manalo"
        echo $NC =========================================
}
#LOOP CONDITION
quit=Y
while [[ "$quit" != N ]]&&[[ "$quit" != n ]]
do
        macinfo
        read -p "Do you want to display it again? Y/N? " quit
done
